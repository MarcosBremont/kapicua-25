﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kapicua25.Objetos
{
    public class EGlobal
    {
        public string Equipo1 { get; set; }
        public string Equipo1Jugador1 { get; set; }
        public string Equipo1Jugador2 { get; set; }
        public string Equipo2 { get; set; }
        public string Equipo2Jugador1 { get; set; }
        public string Equipo2Jugador2 { get; set; }
        public string Tantos { get; set; }
        public string Ganador { get; set; }

    }
}
